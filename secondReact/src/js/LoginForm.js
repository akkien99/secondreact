import React, { Component } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Text } from 'react-native';


export default class LoginForm extends Component{
    render(){
        return(
            <View style = {styles.container}>
                
                <TextInput 
                    underlineColorAndroid='transparent'
                    placeholder = 'username'
                    placeholderTextColor = 'rgba(255, 255, 255, 0.7)'
                    returnKeyType = 'next'
                    onSubmitEditing = {() => this.passwordInput.focus()}
                    keyboardType = 'email-address'
                    autoCapitalize= 'none'
                    autoCorrect = {false}
                    style = {styles.input}
                />
                <TextInput 
                    underlineColorAndroid='transparent'
                    placeholder = 'password'
                    placeholderTextColor = 'rgba(255, 255, 255, 0.7)'
                    returnKeyType = 'go'
                    secureTextEntry
                    ref = {(input) => this.passwordInput = input}
                    style = {styles.input}
                />

                <TouchableOpacity style = {styles.buttonContainer}>
                    <Text style = {styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    input: {
        width: 200,
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 10,
        color: '#FFF',
        paddingHorizontal: 10
    },
    buttonContainer: {
        backgroundColor: '#2980b9',
        paddingVertical: 15
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFF',
        fontWeight: '700'
    }
});