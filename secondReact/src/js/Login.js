import React, {Component} from 'react'
import {StyleSheet, View, Image, Text, KeyboardAvoidingView } from 'react-native'
import LoginForm from './LoginForm';

export default class Login extends Component{
    render(){
        return(
            //<KeyboardAvoidingView behavior = 'padding' style = {styles.containter}>
                <View style = {styles.containter}>
                    <View style = {styles.logoContainer}>
                        <Image
                            source = {require('../images/octocat.png')}
                            style = {styles.logo}
                        />

                        <Text style = {styles.title}> I'm dodo, haha </Text>
                        <View style = {styles.formContainer}>
                            <LoginForm/>
                        </View>
                    </View>
                </View>
            //</KeyboardAvoidingView>
        );
    }
}



const styles = StyleSheet.create({
    containter: {
        flex: 1,
        backgroundColor: '#3498db',
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo:{
        width: 100,
        height: 100
    },
    title: {
        color: '#FFF',
        marginTop: 10,
        width: 160,
        textAlign: 'center',
        opacity: 0.9
    }
});