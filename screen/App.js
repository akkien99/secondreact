import {
	createStackNavigator,
} from 'react-navigation';

//import HomeScreen from './component/Home';
import ActivityScreen from './component/Screen1';
import CategoryScreen2a from './component/Screen2a';
import CategoryScreen2b from './component/Screen2b';

const App = createStackNavigator({
	//Home: { screen: HomeScreen },
	Activity: { screen: ActivityScreen },
	Category2a: { screen: CategoryScreen2a },
	Category2b: { screen: CategoryScreen2b }
});

export default App;








