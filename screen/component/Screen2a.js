import React, { Component } from 'react';
import {StyleSheet, View, Text, Button, Alert} from 'react-native';


export default class ProfileScreen extends Component {

  constructor (props){
		super(props);
	
		this.confirm = this.confirm.bind(this);
	}

  static navigationOptions = {
    title: 'Chọn phân',
	};
	
	confirm(message){
		Alert.alert(
			'Xác nhận',
			'Bạn có xác nhận đã dùng phân ' + message,
			[
				//{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
				{text: 'Không', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
				{text: 'Có', onPress: () => console.log('OK Pressed')}
			],
			{ cancelable: false }
		);
	}

    render() {
    	const { navigate } = this.props.navigation;
    	return (
			<View style = {{flex:1, flexDirection: 'column', backgroundColor: 'skyblue'}}>
				<View style = {{flex:1, justifyContent:'center', alignItems:'center', backgroundColor: 'steelblue'}}>
					<Text style = {{color: '#0f0', fontSize: 20 }}>Bạn bón loại phân nào?</Text>
				</View>
				<View style={{
					flex: 5,
					flexDirection: 'column',
					justifyContent: 'space-around',
					alignItems: 'center'
				}}>
					<Button
					title="NPK 16-16-8"
					onPress={() => this.confirm('NPK 16-16-8')}
					/>
					<Button
					title="Phân chuồng"
					onPress={() => this.confirm('chuồng')}
					/> 
          <Button
					title="Urê"
					onPress={() => this.confirm('Urê')}
					/>
          <Button
					title="Agrotain"
					onPress={() => this.confirm('NPK 16-16-8')}
					/> 
				</View>

				<View style={{
					flex: 1,
					backgroundColor: 'steelblue'
				}}>
				</View>

			</View>
		);
    }
}


const styles = StyleSheet.create({
	
});