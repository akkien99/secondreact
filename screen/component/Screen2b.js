import React, { Component } from 'react';
import {StyleSheet, View, Text, Button, Alert} from 'react-native';


export default class ProfileScreen extends Component {
    static navigationOptions = {
    	title: 'Chọn thuốc',
	};
	
	confirm(message){
		Alert.alert(
			'Xác nhận',
			'Bạn có xác nhận đã dùng thuốc ' + message,
			[
				//{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
				{text: 'Không', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
				{text: 'Có', onPress: () => console.log('OK Pressed')}
			],
			{ cancelable: false }
		);
	}


	render() {
		const { navigate } = this.props.navigation;
		return (

		<View style = {{flex:1, flexDirection: 'column', backgroundColor: 'skyblue'}}>
			<View style = {{flex:1, justifyContent:'center', alignItems:'center', backgroundColor: 'steelblue'}}>
				<Text style = {{color: '#0f0', fontSize: 20 }}>Bạn phun loại thuốc nào?</Text>
			</View>
			<View style={{
				flex: 5,
				flexDirection: 'column',
				justifyContent: 'space-around',
				alignItems: 'center'
			}}>
				<Button
				title="Malate 73 EC"
				onPress={() => this.confirm('Malate 73 EC')}
				/>
				<Button
				title="Actara 25 WG"
				onPress={() => this.confirm('Actara 25 WG')}
				/>
				<Button
				title="Admire 050 EC"
				onPress={() => this.confirm('Admire 050 EC')}
				/> 
				<Button
				title="Cyrux 5EC"
				onPress={() => this.confirm('Cyrux 5EC')}
				/> 
			</View>

			<View style={{
				flex: 1,
				backgroundColor: 'steelblue'
			}}>
			</View>

		</View>
		);
	}
}


const styles = StyleSheet.create({
	
});