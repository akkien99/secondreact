import React, { Component } from 'react';
import {StyleSheet, View, Text, Button, Alert} from 'react-native';


export default class HomeScreen extends Component {
	constructor (props){
		super(props);
		
		this.confirm = this.confirm.bind(this);
	}
    static navigationOptions = {
    	title: 'Chọn hoạt động',
	};
	confirm(message){
		Alert.alert(
			'Xác nhận',
			'Bạn có xác nhận đã ' + message,
			[
				//{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
				{text: 'Không', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
				{text: 'Có', onPress: () => console.log('OK Pressed')}
			],
			{ cancelable: false }
		);
	}
    render() {
    	const { navigate } = this.props.navigation;
    	return (
			<View style = {{flex:1, flexDirection: 'column'}}>
				<View style = {{flex:1, justifyContent:'center', alignItems:'center', backgroundColor: 'steelblue'}}>
					<Text style = {{color: '#0f0', fontSize: 20 }}>Hôm nay bạn làm gì?</Text>
				</View>
				<View style={{
					flex: 5,
					flexDirection: 'row',
					//justifyContent: 'space-around',
				}}>

					<View style={styles.buttonContainer} >
						<Button
							style = {{width: 800}} //?
							title="Tưới nước"
							onPress = {() => this.confirm('tưới nước')}
						/>
						<Button
							title="Bón phân"
							onPress={() =>
								navigate('Category2a', { name: 'Jane' })
							}
						/>
						<Button
							title="Xịt thuốc"
							onPress={() =>
								navigate('Category2b', { name: 'Jane' })
							}
						/>					
					</View>
					<View style={styles.buttonContainer} >
						<Button
							title="Bao trái"
							onPress = {() => this.confirm('bao trái')}							
						/>
						<Button
							title="Tỉa cành"
							onPress = {() => this.confirm('tỉa cành')}
						/>
						<Button
							title="Thu hoạch"
							onPress = {() => this.confirm('thu hoạch')}
						/>
					</View>
					
				</View>

				<View style={{
					flex: 1,
					backgroundColor: 'steelblue'
				}}>
				</View>

        	</View>
    	);
    }
}

const styles = StyleSheet.create({
	buttonContainer: {
		flex: 1,
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: 'skyblue'
	}
});